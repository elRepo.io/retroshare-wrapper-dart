part of rs_models;

class Account {
  String locationId;
  String pgpId;
  String locationName;
  String pgpName;
  Account(this.locationId, this.pgpId, this.locationName, this.pgpName);
}
