part of rs_models;

class Location {
  String rsPeerId;
  String rsGpgId;
  String accountName;
  String locationName;
  bool isOnline;

  Location(
    this.rsPeerId,
    this.rsGpgId,
    this.accountName,
    this.locationName,
    [this.isOnline = false]
  );

  Location.fromJson(Map<String, dynamic> jsonString) : this(
    jsonString['mLocationId'],
    jsonString['mPgpId'],
    jsonString['mLocationName'],
    jsonString['mPgpName'],
  );
}
