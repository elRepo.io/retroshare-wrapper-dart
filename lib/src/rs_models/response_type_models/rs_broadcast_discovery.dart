/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2022  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2022 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

part of rs_models;

class BroadcastDiscoveryPeer{
  late String mPgpFingerprint;
  late String mSslId;
  late String mProfileName;
  late Map<String, dynamic> mLocator;

  BroadcastDiscoveryPeer.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    mPgpFingerprint = json['mPgpFingerprint'];
    mSslId = json['mSslId'];
    mProfileName = json['mProfileName'];
    mLocator = json['mLocator'];
  }

  String get pgpId => mPgpFingerprint.substring(mPgpFingerprint.length - 16);

  /// Return a parsed uri for the [mLocator]
  ///
  /// Resulting object on format, used when call `addSslOnlyFriend`
  ///
  /// ```
  /// {
  ///      'localAddr': mLocator.urlString.host,
  ///      'localPort': mLocator.urlString.port
  /// };
  /// ```
  Map<String, dynamic> get mLocatorDetails {
    final parsedUri = Uri.parse(mLocator['urlString']!);
    return {
      'localAddr': parsedUri.host,
      'localPort': parsedUri.port
    };
  }
}