/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2022  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2022 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

part of rs_models;

class RsGxsForumMsg {

  late RsMsgMetaData mMeta;
  late String mMsg;
  RsGxsForumMsg();

  @override
  String toString() {
    return 'RsGxsForumMsg[mMeta=$mMeta, mMsg=$mMsg, ]';
  }

  RsGxsForumMsg.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    mMeta = RsMsgMetaData.fromJson(json['mMeta']);
    mMsg = json['mMsg'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (mMeta != null)
      json['mMeta'] = mMeta;
    if (mMsg != null)
      json['mMsg'] = mMsg;
    return json;
  }

  static List<RsGxsForumMsg> listFromJson(List<dynamic> json) {
    return json == null ? <RsGxsForumMsg>[] : json.map((value) => RsGxsForumMsg.fromJson(value)).toList();
  }

  static Map<String, RsGxsForumMsg> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsGxsForumMsg>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsGxsForumMsg.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsGxsForumMsg-objects as value to a dart map
  static Map<String, List<RsGxsForumMsg>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsGxsForumMsg>>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) {
        map[key] = RsGxsForumMsg.listFromJson(value);
      });
    }
    return map;
  }
}
