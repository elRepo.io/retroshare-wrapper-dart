/*
 *  elRepo.io decentralized culture repository
 *
 *  Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 *  This program is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Affero General Public License as published by the
 *  Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 *  SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 *  SPDX-License-Identifier: AGPL-3.0-only
 *
 */

part of rs_models;

class DirDetails {
  List parent_groups;

  /// Json with keys: type, name, handle
  ///
  /// Where type 4 is for directories and 8 for files, where handle is the next
  /// handle used to navigate into it.
  List<Map<String, dynamic>> children;

  RsInt64 handle;
  RsInt64 parentHandle;

  /// parent row
  int prow;
  int type;
  String id;
  String name;
  String hash;
  String path;
  RsInt64 size;
  int mtime;
  int flags;
  int max_mtime;

  DirDetails({
    required this.parent_groups,
    required this.children,
    required this.handle,
    required this.parentHandle,
    required this.prow,
    required this.type,
    required this.id,
    required this.name,
    required this.hash,
    required this.path,
    required this.size,
    required this.mtime,
    required this.flags,
    required this.max_mtime,
  });

  DirDetails.fromJson(Map<String, dynamic> jsonString)
      : this(
          parent_groups: jsonString['parent_groups'],
          children:
              jsonString['children'].cast<Map<String, dynamic>>().toList(),
          handle: RsInt64.fromJson(jsonString['handle']),
          parentHandle: RsInt64.fromJson(jsonString['parentHandle']),
          prow: jsonString['prow'],
          type: jsonString['type'],
          id: jsonString['id'],
          name: jsonString['name'],
          hash: jsonString['hash'],
          path: jsonString['path'],
          size: RsInt64.fromJson(jsonString['size']),
          mtime: jsonString['mtime'],
          flags: jsonString['flags'],
          max_mtime: jsonString['max_mtime'],
        );
}


enum RetroShareFileDownloadStatus {
  FT_STATE_FAILED,
  FT_STATE_OKAY,
  FT_STATE_WAITING, // Waiting download to start
  FT_STATE_DOWNLOADING, // Download started
  FT_STATE_COMPLETE, //Download complete
  FT_STATE_QUEUED,
  FT_STATE_PAUSED,
  FT_STATE_CHECKING_HASH,
}

/// Used to control file download
class RS_FILE_CTRL {
  static final int PAUSE	 		      = 0x00000100;
  static final int START	 		      = 0x00000200;
  static final int FORCE_CHECK	    = 0x00000400;
}
