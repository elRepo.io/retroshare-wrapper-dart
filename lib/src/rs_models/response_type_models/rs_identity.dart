part of rs_models;

class Identity {
  /// RsGxsId
  // RsPgpID
  // String pgpId;  not used
  late String name;
  late String mId;

  // Below attributes got with /rsIdentity/getIdentitiesInfo
  // todo(kon): move this to a RsGxsImage
  String _avatar = "";
  int? avatarSize;

  // bool signed; not used
  bool isContact = false;
  RsInt64? mLastUsageTS, mPublishTs;

  /// Will store raw Json from /rsIdentity/getIdentitiesInfo
  Map<String, dynamic> rawIdentityInfo = {};
  /// Will store raw Json from /rsIdentity/getIdentitiesSummaries
  Map<String, dynamic> rawIdentitySummary = {};

  set avatar(String avatar) {
    _avatar = avatar;
  }

  String get avatar => _avatar;

  Identity(this.mId,
      [String? name, String avatar = "", this.isContact = false,]) {
    this.name = name ?? mId;
    _avatar = avatar;
  }

  /// Instantiate an identity from json resulting when getIdentitiesSummaries
  /// is called.
  ///
  /// Avatar will be empty because is not there
  Identity.fromGetIdentitiesSummaries(Map<String, dynamic> jsonIdSummary){
    rawIdentitySummary = jsonIdSummary;
    mId = jsonIdSummary["mGroupId"];
    name = jsonIdSummary["mGroupName"];
  }

  /// getIdentitiesInfo Identity constructor
  Identity.fromGetIdentitiesInfo(Map<String, dynamic> idsInfo) {
    rawIdentityInfo = idsInfo;
    avatar = idsInfo['mImage']['mData']['base64'];
    avatarSize = idsInfo['mImage']['mSize'];
    name = idsInfo['mMeta']['mGroupName'];
    mId = idsInfo['mMeta']['mGroupId'];
    isContact = idsInfo['mIsAContact'];
    mLastUsageTS = RsInt64.fromJson(idsInfo['mLastUsageTS']);
    mPublishTs = RsInt64.fromJson(idsInfo['mMeta']['mPublishTs']);
  }


  /// Update an identity from other. Util when need to merge two identities
  /// (ex: [fromGetIdentitiesInfo] with [fromGetIdentitiesSummaries]).
  ///
  /// The only not updated thing is the [mId] because they have to be the same
  void update (Identity id) {
    if (id.mId == id) {
      rawIdentityInfo = id.rawIdentityInfo.isNotEmpty
          ? id.rawIdentityInfo : rawIdentityInfo;
      rawIdentitySummary = id.rawIdentitySummary.isNotEmpty
          ? id.rawIdentitySummary : rawIdentitySummary;
      avatar = id.avatar.isNotEmpty
          ? id.avatar : avatar;
      avatarSize = id?.avatarSize;
      name = id.name.isNotEmpty
          ? id.name : name;
      isContact = id.isContact;
      mLastUsageTS = id?.mLastUsageTS;
      mPublishTs = id?.mPublishTs;
    }
  }
}
