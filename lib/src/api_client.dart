/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2022  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2022 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

part of retroshare;

class RsApiClient {

  RsApiClient({this.basePath = RETROSHARE_SERVICE_PREFIX,});

  String basePath;

  String get retroshareServicePrefix => basePath;

  set retroshareServicePrefix(String? prefix) =>
      basePath = prefix ?? RETROSHARE_SERVICE_PREFIX;

  /// For a specific [path] append the [retroshareServicePrefix]
  String getCompletePath (String path) => retroshareServicePrefix + path;

  var _client = Client();

  /// Returns the current HTTP [Client] instance to use in this class.
  ///
  /// The return value is guaranteed to never be null.
  Client get client => _client;

  /// Requests to use a new HTTP [Client] in this class.
  set client(Client newClient) {
    _client = newClient;
  }

  /// This callback will be called when RetroShare have to be restarted
  ///
  /// For example when retroshare is down and need to be restarted
  Function? _rsStartCallback;

  /// Set the retroshare restart callback. Called when executing [restartRSIfDown]
  ///
  /// This callback is automatically executed trying to restart RetroShare if down
  set rsStartCallback(Function? callback) {
    _rsStartCallback = callback;
  }

  Function? get rsStartCallback => _rsStartCallback;

  /// Returns an authentication header to use for RS
  String makeAuthHeader(String username, String? password) =>
      'Basic ' + base64Encode(utf8.encode('$username:$password'));

  /// Call the given RetroShare JSON API method with given paramethers, and return
  /// results, raise exceptions on errors.
  /// Path is expected to contain a leading slash "/" and params is expected to
  /// be serializable to JSON.
  Future<Map<String, dynamic>> apiCall(
      String path, {
        Map<String, dynamic> params = const {},
        String? basicAuth,
      }) async {

    var reqUrl = getCompletePath(path);

    basicAuth ??=
        makeAuthHeader(rsLocalState.authApiUser, rsLocalState.authPassphrase);

    try {
      final response = await _client.post(Uri.parse(reqUrl),
          body: jsonEncode(params),
          headers: <String, String>{'Authorization': basicAuth});

      // This mean that probably RS service is down.
      if (response == null || !(response.statusCode is int)) {
        // If restarted successfully try to call the API call again.
        // This is dangerous, could enter to eternal recursive function.
        // todo(kon): prevent recursive function without exit
        if (await restartRSIfDown("restartRSIfDown[APICALL]" + path)) {
          return apiCall(path, params: params, basicAuth: basicAuth);
        }
      }

      // Can happen this with status code? I don't think so
      if (response == null) throw Exception('Request failed: ' + reqUrl);

      switch (response.statusCode) {
        case 200:
          return jsonDecode(utf8.decode(response.bodyBytes));
        case 409: // This code is thrown when is not logged in
          if (!(await RsLoginHelper.isLoggedIn())) {
            if (rsLocalState.authLocationId != null && rsLocalState.authPassphrase != null) {
              // If you do the login try to call again the Api call that throw the previous error
              if (await RsLoginHelper.login() == 0) {
                return apiCall(path, params: params, basicAuth: basicAuth);
              }
            }
            throw LoginException(reqUrl);
          }
          throw ApiUnhandledErrorException(response.statusCode, reqUrl);
        default:
          throw Exception(
              statusCodeErrorMessages(response.statusCode, path, reqUrl));
      }
    } catch (err) {
      if (err is SocketException || err is ClientException) {
        // If RS is down throw a SocketException.
        // Probably this can replace the line above that do the same
        if (await restartRSIfDown("restartRSIfDown[APICALL_CATCH] " + err.toString() + " " + path)) {
          return apiCall(path, params: params, basicAuth: basicAuth);
        }
      }
      rethrow;
    }
  }


  /// Register Event
  ///
  /// Where [eventType] is the enum type [RsEventType] that specifies what kind
  /// of event are we listening to.
  ///
  /// The callback return this StreamSubscription object and the Json response
  Future<StreamSubscription<Event>> registerEventsHandler(
      RsEventType eventType,
      Function(StreamSubscription<Event>, Map<String, dynamic>) callback,
      {
        Function? onError,
        String? basicAuth
      }) async {
    await restartRSIfDown("restartRSIfDown[EVENT_HANDLER]" + eventType.name);
    basicAuth ??=
        makeAuthHeader(rsLocalState.authApiUser, rsLocalState.authPassphrase);

    var body = {'eventType': eventType.index};
    var path = '/rsEvents/registerEventsHandler';
    var reqUrl = retroshareServicePrefix + path;

    print('Register event for: ${jsonEncode(body)}');

    StreamSubscription<Event>? streamSubscription;
    try {
      var eventSource = await EventSource.connect(
        reqUrl,
        method: 'GET',
        body: jsonEncode(body),
        headers: {HttpHeaders.authorizationHeader: basicAuth},
      );

      streamSubscription = eventSource.listen((Event event) {
        // Deserialize the message
        var json = event.data != null ? jsonDecode(event.data ?? '') : null;
        if (json['event'] != null && callback != null && streamSubscription != null) {
          callback(streamSubscription, json['event']);
        }
      });
      streamSubscription.onError(onError);
    } on EventSourceSubscriptionException catch (e) {
      print('registerEventsHandler error: ' + e.message);
      throw (statusCodeErrorMessages(e.statusCode, path, reqUrl));
    }

    // Store the subscription on a dictionary
//    rsEventsSubscriptions ??= Map();
//    rsEventsSubscriptions[eventType] = streamSubscription;
    return streamSubscription;
  }

  /// Function to check if Retroshare is running
  ///
  /// Return true if receive a response from the server, false otherwise.
  Future<bool> isRetroshareRunning() async {
    try {
      // This commented lines below are the old implementation, deprecated when
      // developing the web version of elrepo.io due a CORS error.
      // final reqUrl = getRetroshareServicePrefix();
      // final response = await http.post(Uri.parse(reqUrl));
      // return response != null && response.statusCode is int;
      // This is a call to an authenticated method that will show if JsonAPI is on
      // For some reason there is no CORS error when call this method
      final response = await _client.post(Uri.parse(retroshareServicePrefix + '/rsJsonApi/version'),
        body: jsonEncode({}),);
      return response != null && response.statusCode is int && response.statusCode == 200;
    } catch (err) {
      print(err);
      return false;
    }
  }

  /// True if [rsStartCallback] is on execution
  bool _rsIsRestarting = false;

  /// Check if RS is running, try to restart it otherwise
  ///
  /// It try to restart it using the [rsStartCallback] that have to be set before
  /// using [setStartCallback].
  ///
  /// Return true if success throw exception if not. If [rsStartCallback] is null
  /// return false to prevent an infinite restart loop
  Future<bool> restartRSIfDown(String message) async {
    print(message);
    if (_rsStartCallback == null) {
      print('_rsStartCallback not defined');
      return false;
    };
    if (!(await isRetroshareRunning())) {
      print('RS service is down. Restarting');
      try {
        if (!_rsIsRestarting) {
          _rsIsRestarting = true;
          await _rsStartCallback!();
          _rsIsRestarting = false;
        }
      } catch (error) {
        throw Exception('Failed to start RS service. $error');
      }
    }
    return true;
  }
}
