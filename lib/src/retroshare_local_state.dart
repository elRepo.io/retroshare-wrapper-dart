/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2022  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2022 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

part of retroshare;

class RsLocalState {
  String? authLocationId;
  String? authIdentityId;
  String? authPassphrase;
  // TODO: G10h4ck PGP password and API password might be different!
  String? authLocationName;
  String _authApiUser = RETROSHARE_API_USER;

  String get authApiUser => _authApiUser;
  set authApiUser (String? apiUser) {
    _authApiUser = apiUser ?? RETROSHARE_API_USER;
  }

  Location? defaultLocation; // Store default location data if login success

  RsLocalState();

  /// Set internal var
  void initRetroshare({
    String? locationId,
    String? identityId,
    String? passphrase,
    String? apiUser = RETROSHARE_API_USER
  }) {
    authIdentityId = identityId ?? authIdentityId;
    authLocationId = locationId ?? authLocationId;
    authPassphrase = passphrase ?? authPassphrase;
    authApiUser = apiUser;
  }
}
