/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2022  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

part of retroshare;

/// Used to print debug and error messages without polluting stdout which is used
/// by scripts to ouput actual results
void dbg(String msg)
{
  stderr.writeln(msg);
}

String errToStr(Map<String, dynamic> cxx_std_error_condition) {
  var err = cxx_std_error_condition;
  return "${err["errorCategory"]} ${err["errorNumber"]} ${err["errorMessage"]}";
}

RsApiClient rsClient = RsApiClient();
RsLocalState rsLocalState = RsLocalState();

// ////////////////////////////////////////////////////////////////////////////
// / SPECIFIC RETROSHARE OPERATIONS
// ////////////////////////////////////////////////////////////////////////////

class RsLoginHelper {
  static Future<bool> hasLocation() => getLocations()
      .then((locations) => locations is List && locations.isNotEmpty);

  /// Return map for last used location
  static Future<Location?> getDefaultLocation() async {
    var current = await RsAccounts.getCurrentAccountId();
    if(!current.startsWith("000000000")) {
      for (var location in await getLocations()) {
        if (location.rsPeerId == current) return location;
      }
    }
    return null;
  }

  /// Creates a Retroshare Account
  /// Returns the location to use the account from now on
  static Future<Location> createLocation(String locationName, String password,
      {String? api_user}) async {
    final mPath = '/rsLoginHelper/createLocationV2';
    final mParams = {
      'locationName': locationName,
      'pgpName': locationName,
      'password': password,
      'apiUser': api_user ?? RETROSHARE_API_USER,
      /* TODO(G10h4ck): The new token scheme permit arbitrarly more secure
       * options to avoid sending PGP password at each request. */
      'apiPass': password
    };
    final response = await rsClient.apiCall(mPath, params: mParams);

    if (!(response is Map)) {
      throw FormatException('response is not a Map');
    } else if (response['retval']['errorNumber'] != 0) {
      throw Exception('Failure creating location: ' + jsonEncode(response));
    } else if (!(response['locationId'] is String)) {
      throw FormatException('location is not a String');
    }

    return Location(
      response['locationId'],
      response['pgpId'],
      locationName,
      locationName,
    );
  }

  /// Login in function
  ///
  /// You can send custom [location] and [password] to configure the global ones
  /// defined in [authLocationId] and [authPassphrase].
  ///
  /// If is already logged in it execute [RsIdentity.getOrCreateIdentity].
  ///
  /// Else do an `/rsLoginHelper/attemptLogin`.
  ///
  /// Return 0 if everything is Ok
  // todo(kon) : refator this function do strange stuff
  static Future<int> login([Location? location, String? password]) async {
    if (location != null) {
      rsLocalState.authLocationName = location.locationName;
      rsLocalState.authLocationId = location.rsPeerId;
    }
    if (password != null) {
      rsLocalState.authPassphrase = password;
    }
    if (rsLocalState.authPassphrase == null || rsLocalState.authLocationId == null) {
      throw Exception(
          'No credentials provided and no previous login was registered');
    }
    if (await isLoggedIn()) {
      // Handle error if its already logged in
      try {
        rsLocalState.authIdentityId = await RsIdentity.getOrCreateIdentity();
        return 0;
      } catch (e) {
        print(e);
        return 3;
      }
    }

    var response = await rsClient.apiCall('/rsLoginHelper/attemptLogin', params: {
      'account': rsLocalState.authLocationId,
      'password': rsLocalState.authPassphrase,
    });

    if (!(response is Map)) throw FormatException();
    switch (response['retval']) {
      case 0:
        rsLocalState.defaultLocation = location;
        rsLocalState.authIdentityId = await RsIdentity.getOrCreateIdentity();
        print('location/identity in use: ${rsLocalState.authLocationId} / ${rsLocalState.authIdentityId}');
        return 0;
      case 1: // ERR_ALREADY_RUNNING
        throw Exception('Already running');
      case 2: // ERR_CANT_ACQUIRE_LOCK
        throw Exception('The account is already in use');
      case 3: // ERR_UNKNOWN
        print('Could not decrypt the account data');
        return response['retval'];
      default:
        print('Could not log in');
        return response['retval'];
    }
  }

  /// Check if is logged in
  ///
  /// Return false if `/rsLoginHelper/isLoggedIn` is false and also if one of
  /// the variables [authLocationId] or [authPassphrase] is null, (what mean
  /// that are not set).
  static Future<bool> isLoggedIn() async {
    var response = await rsClient.apiCall('/rsLoginHelper/isLoggedIn');
    if (!(response is Map)) throw FormatException();

    final loggedIn = (response['retval'] == true) ||
        (response['retval'] ==
            1); // todo(kon): Why? Compatibility between version API's maybe?
    return (loggedIn && rsLocalState.authLocationId != null && rsLocalState.authPassphrase != null);
  }

  static Future<List<Location>> getLocations() async {
    var response = await rsClient.apiCall('/rsLoginHelper/getLocations');

    if (!(response is Map)) throw FormatException();

    return [for(Map<String, dynamic> location in response['locations']) Location.fromJson(location)];
  }
}

class RsAccounts {

  /// Get current account id. Beware that an account may be selected without
  /// actually logging in.
  ///
  /// Return id storage for current account id. If account hasn't been selected
  /// yet, return `0000000000000000000000000000000`
  static Future<String> getCurrentAccountId() async {
    var response = await rsClient.apiCall('/rsAccounts/getCurrentAccountId');
    return response['id'];
  }

  /// ConfigDirectory (usually ~/.retroshare) you can call this method
  /// Even before initialisation (you can't with some other methods)
  static Future<String> configDirectory() async {
    var response = await rsClient.apiCall('/rsAccounts/configDirectory');
    if (response['retval'] == null) throw ("Can't configDirectory retval is not true");
    return response['retval'];
  }
}

class RsControl {
  /// Turn off RetroShare
  // todo(kon): unimplemented
  static Future<void> rsGlobalShutDown () async =>
     (await rsClient.apiCall('/rsAccounts/getCurrentAccountId'));
}

class RsIdentity {
  /// Create a new identity
  ///
  /// Return [id] storage for the created identity Id
  /// [name] Name of the identity
  /// NOT SUPORTED avatar Image associated to the identity
  /// NOT SUPORTED (default true) [pseudonimous] true for unsigned identity, false otherwise
  /// [pgpPassword] password to unlock PGP to sign identity, not implemented yet
  /// @return false on error, true otherwise
  static Future<String /*gxsId*/ > createIdentity(
      String name, {bool pseudonimous = true, String pgpPassword = ""} ) async
  {
    final mPath = '/rsIdentity/createIdentity';
    final mParams = {
      'name': name,
      'pseudonimous': pseudonimous,
      'pgpPassword': pgpPassword
    };
    final response = await rsClient.apiCall(mPath, params: mParams);

    if (response['retval'] != true) throw Exception('Error creating identity');
    if (!(response['id'] is String)) throw Exception('Invalid response ID');

    // Store auth state
    return response['id'];
  }

  static Future<List<String /*gxsId*/>> getOwnSignedIds() async
  {
    final mPath = '/rsIdentity/getOwnSignedIds';
    final response = await rsClient.apiCall(mPath);

    if (response['retval'] != true)
      throw Exception('Error calling $mPath');

    List<String> gxsIds = [];
    response['ids'].forEach((gxsId) => gxsIds.add(gxsId));

    return gxsIds;
  }

  // Returns the ID of the default identity or creates one if there is none defined
  static Future<String?> getOrCreateIdentity() async
  {
    if (rsLocalState.authIdentityId is String && rsLocalState.authIdentityId!.length > 1)
    {
      return rsLocalState.authIdentityId;
    }

    try
    {
      // Request it
      final mPath = '/rsIdentity/getOwnPseudonimousIds';
      final response = await rsClient.apiCall(mPath);

      if (!(response['ids'] is List))
      {
        throw Exception('Invalid response IDs');
      }
      else if (response['ids'].length < 1)
      {
        print('No identity yet. Creating one.');
        final newIdentityId =
            await RsIdentity.createIdentity(rsLocalState.authLocationName!);

        if (!(newIdentityId is String) || newIdentityId.isEmpty)
        {
          throw Exception('Could not create Id');
        }
        else
        {
          print('New Id: $newIdentityId');
          return newIdentityId;
        }

        // we have and id, so we return it
      }
      else
      {
        print("Existing Ids ${response['ids']}");
        return response['ids'][0];
      }
    }
    catch (err)
    {
      print('Could not get Own Ids. Error: $err');
      rethrow;
    }
  }

  static Future<bool> isKnownId(String sslId) async {
    try {
      final mPath = '/rsIdentity/isKnownId';
      final mParams = {'id': sslId};
      final response = await rsClient.apiCall(mPath, params: mParams);

      return response['retval'] == true;
    } catch (err) {
      return false;
    }
  }
  /// Request details of a not yet known identity to the network
  ///
  /// [sslId] id of the identity to request
  ///
  /// NOT SUPPORTED [peers] optional list of the peers to ask for the key, if
  /// empty all online peers are asked.
  ///
  /// return false on error, true otherwise
  static Future<void> requestIdentity(String sslId) async {
    try {
      final mPath = '/rsIdentity/requestIdentity';
      final mParams = {'id': sslId};
      final response = await rsClient.apiCall(mPath, params: mParams);

      if (response['retval'] != true) throw Exception("/rsIdentity/requestIdentity retval is not true $response");
    } catch (err) {
      rethrow;
    }
  }

  /// This is deprecated, better use [getIdentitiesInfo]
  @Deprecated('migration')
  static Future<Map> getIdDetails(String identityId) async {
    Map identityDetails;
    final mPath = '/rsIdentity/getIdDetails';
    final mParams = {'id': identityId};
    var response = await rsClient.apiCall(mPath, params: mParams);

    if (response['retval'] != true) {
      // TODO(nicoechaniz): for some reason RS is not getting this right all the time and response["details"] responds with
      // a json with the structure byt no data, so we repeat with a delay. Check upstream if this is intended.
      await Future.delayed(Duration(seconds: 2));
      response = await rsClient.apiCall(mPath, params: mParams);
    }
    if (response['retval'] != true) {
      throw Exception('Could not retrieve details for id $identityId');
    }

    identityDetails = response['details'];
    return identityDetails;
  }

  ///  Get identities summaries list.
  ///
  /// Return  [ids] list where to store the identities
  static Future<List<Identity>> getIdentitiesSummaries() async {
    final response = await rsClient.apiCall('/rsIdentity/getIdentitiesSummaries');
    if (response['retval'] != true) {
      throw Exception('Could not retrieve identities summaries');
    }
    return [for(Map<String, dynamic> id in response['ids']) Identity.fromGetIdentitiesSummaries(id)];
  }

  /// Get identities information (name, avatar...).
  ///
  /// [ids] ids of the channels of which to get the informations
  /// return [idsInfo] storage for the identities informations
  static Future<List<Identity>> getIdentitiesInfo(List<String> ids) async {
    // todo(aruru): do this and other attribute checks on all calls that need it
    if(ids.isEmpty) return [];
    final response =
        await rsClient.apiCall('/rsIdentity/getIdentitiesInfo', params: {'ids': ids});
    if (!(response is Map)) {
      throw Exception('Could not retrieve the details on getIdentitiesInfo');
    } else if (response['retval'] != true) {
      throw Exception("Can't retrieve getIdentitiesInfo for $ids");
    }
    return [for(Map<String, dynamic> id in response['idsInfo']) Identity.fromGetIdentitiesInfo(id)];

  }

  /// Update identity data (name, avatar...)
  ///
  /// id Id of the identity
  /// name New name of the identity
  /// Optional [avatar] New avatar for the identity in RsGxsImage format.
  ///
  /// NOT SUPORTED [pseudonimous] Set to true to make the identity anonymous.
  /// If set to false, updating will require the profile passphrase.
  /// [pgpPassword] Profile passphrase, if non pseudonymous.
  /// @return false on error, true otherwise
  static Future<bool> updateIdentity(
      String id, String name, [ RsGxsImage? avatar, ]) async {
    print('Updating identity $id');
    var params = <String, dynamic>{
      'id': id,
      'name': name,
      'pseudonimous': true,
    };
    if (avatar != null) params['avatar'] = avatar.toJson();

    var response =
        await rsClient.apiCall('/rsIdentity/updateIdentity', params: params);

    print('IdentityUpdate result: $response');

    if (response['retval'] != true) throw ("Can't update the Identity $id");

    return response['retval'];
  }

  /// Set/unset identity as contact
  ///
  /// @param[in] [id] Id of the identity
  /// @param[in] [isContact] true to set, false to unset
  /// @return false on error, true otherwise
  static Future<bool> setAsRegularContact(String id, bool isContact) async {
    final response = await rsClient.apiCall('/rsIdentity/setAsRegularContact',
        params: {'id': id, 'isContact': isContact});
    return response['retval'];
  }
}

// ----------------------------------------------------------------------------
// Join the network
// ----------------------------------------------------------------------------

class RsPeers {
  /// get the certificate/invite for current identity
  static Future<String> getRetroshareInvite() async {
    final mPath = '/rsPeers/GetRetroshareInvite';
    final response = await rsClient.apiCall(mPath);
    return response['retval'];
  }

  /// Get RetroShare short invite of the given peer
  ///
  /// [sslId] Id of the peer of which we want to generate an invite,
  ///	a null id (all 0) is passed, an invite for own node is returned.
  /// NOT SUPPORTED [inviteFlags] specify extra data to include in the invite and
  ///	format.
  /// [baseUrl] URL into which to sneak in the RetroShare invite
  ///	radix, this is primarly useful to trick other applications into making
  ///	the invite clickable, or to disguise the RetroShare invite into a
  ///	"normal" looking web link. Used only if formatRadix is false.
  /// @return false if error occurred, true otherwise
  /// @param[out] invite storage for the generated invite
  static Future<String> getShortInvite({String? sslId, String ?baseUrl}) async {
    var mParams = {
      'sslId': sslId,
    };
    if (baseUrl != null) mParams['baseUrl'] = baseUrl;
    final response =
        await rsClient.apiCall('/rsPeers/GetShortInvite', params: mParams);
    if (!response['retval']) {
      throw Exception('Could not get short invite for $sslId');
    }
    return response['invite'];
  }

  static Future<bool> addSslOnlyFriend(
      String sslId, String pgpId, Map details) async {
    final response = await rsClient.apiCall('/rsPeers/addSslOnlyFriend', params: {'sslId': sslId, 'pgpId': pgpId, 'details': details});
    return response['retval'];
  }

  /// Accepts long invite codes only
  static Future<bool> acceptInvite(String base64Payload) async {
    final mPath = '/rsPeers/acceptInvite';
    final mParams = {'invite': base64Payload};
    final response = await rsClient.apiCall(mPath, params: mParams);

    return response['retval'] == true;
  }

  /// Accepts short invite codes only
  static Future<bool> acceptShortInvite(String shortBase64Payload) async {
    final details = await RsPeers.parseShortInvite(shortBase64Payload);

    final mPath = '/rsPeers/addSslOnlyFriend';
    final params = {
      'sslId': details['id'],
      'pgpId': details['gpg_id'],
      'details': details
    };
    final response = await rsClient.apiCall(mPath, params: params);

    // if (response["retval"] != true)
    //   throw Exception("The invitation could not be accepted");

    // TODO: Fail on RS if public key is not ready

    return response['retval'] == true;
  }

  static Future<void> connectAttempt(String sslId) async {
    final mPath = '/rsPeers/connectAttempt';
    final params = {'sslId': sslId};
    final response = await rsClient.apiCall(mPath, params: params);

    if (response['retval'] != true) {
      throw Exception('The connection attempt could not be completed');
    }
  }

  static Future<Map<String, dynamic>> parseShortInvite(
      String shortBase64Payload) async {
    var mPath = '/rsPeers/parseShortInvite';
    var params1 = {'invite': shortBase64Payload};
    final response = await rsClient.apiCall(mPath, params: params1);

    if (!(response is Map) ||
        (response['retval'] != true && response['retval'] != 1)) {
      throw Exception('Could not parse the short invite code');
    } else if (!(response['details'] is Map) ||
        !(response['details']['id'] is String) ||
        !(response['details']['gpg_id'] is String)) {
      throw Exception('Could not parse the short invite code');
    }

    return response['details'];
  }

  /// Get details details of the given peer
  ///
  /// [peerId] id of the peer
  /// return det storage for the details of the peer
  static Future<Map<String, dynamic>> getPeerDetails(String peerId) async {
    final mPath = '/rsPeers/getPeerDetails';
    final mParams = {'sslId': peerId};

    final response = await rsClient.apiCall(mPath, params: mParams);

    if (response['retval'] != true) {
      throw Exception('The details could not be retrieved');
    } else if (!(response['det'] is Map)) {
      throw Exception('The details are not valid');
    }
    return response['det'];
  }

  /// Get PGP id for the given peer
  ///
  /// [sslId] SSL id of the peer
  /// return PGP id of the peer
  static Future<String> getGPGId(String sslId) async {
    var res =
        await rsClient.apiCall('/rsPeers/getGPGId', params: {'sslId': sslId});
    return res['retval'] == "0000000000000000"
        ? throw Exception('Can\'t get gpg id for $sslId')
        : res['retval'];
  }

  static Future<List<String>> getOnlineList() async {
    final mPath = '/rsPeers/getOnlineList';
    final response = await rsClient.apiCall(mPath);

    if (response['retval'] != true) {
      throw Exception('The list could not be retrieved');
    } else if (!(response['sslIds'] is List)) {
      throw Exception('The list is not valid');
    }

    return response['sslIds'].cast<String>().toList();
  }

  static Future<List<String>> getFriendList() async {
    final mPath = '/rsPeers/getFriendList';
    final response = await rsClient.apiCall(mPath);

    if (response['retval'] != true) {
      throw Exception('The list could not be retrieved');
    } else if (!(response['sslIds'] is List)) {
      throw Exception('The list is not valid');
    }

    return response['sslIds'].cast<String>().toList();
  }

  static Future<List<dynamic>> getGroupInfoList() async {
    final response = await rsClient.apiCall('/rsPeers/getGroupInfoList');
    if (response['retval'] != true) {
      throw Exception('Could not retrieve groups info');
    }
    return response['groupInfoList'];
  }

  static Future<bool> isOnline(String sslId) async {
    final mPath = '/rsPeers/isOnline';
    final params = {'sslId': sslId};
    final response = await rsClient.apiCall(mPath, params: params);

    return response['retval'] == true;
  }

  static Future<bool> isFriend(String sslId) async {
    final response = await rsClient.apiCall('/rsPeers/isFriend', params: {'sslId': sslId});
    return response['retval'];
  }

  static Future<bool> removeFriend(String pgpId) async {
    final mPath = '/rsPeers/removeFriend';
    final params = {'pgpId': pgpId};
    final response = await rsClient.apiCall(mPath, params: params);

    if (response['retval'] != true) {
      throw Exception('Remove friend could not be completed');
    }

    return response['retval'];
  }
}

// ----------------------------------------------------------------------------
// Broadcast Discovery
// ----------------------------------------------------------------------------

class RsBroadcastDiscovery {
  static Future<void> enableMulticastListening() async {
    final response =
        await rsClient.apiCall('/rsBroadcastDiscovery/isMulticastListeningEnabled');
    if (!response['retval']) {
      await rsClient.apiCall('/rsBroadcastDiscovery/enableMulticastListening');
    }
  }

  static Future<List<BroadcastDiscoveryPeer>> getDiscoveredPeers() async {
    try {
      var response = await rsClient.apiCall('/rsBroadcastDiscovery/getDiscoveredPeers');
      return (response['retval'] as List<dynamic>).map(
              (e) => BroadcastDiscoveryPeer.fromJson(e)).toList();
    } catch (error) {
      throw (Exception('Error discovering peers. $error'));
    }
  }
}

// ----------------------------------------------------------------------------
// Individual messaging
// ----------------------------------------------------------------------------

class RsMsgs {
  static const RS_MSG_PENDING = 0x0002;

  /// Sends a private message (payload) to the node(s) from the list and returns
  /// an array with the delivery ID
  static Future<List<String>> sendMail(
      List<String> to, Map<String, dynamic> payload) async {
    if (!(to is List) || to.isEmpty) {
      return [];
    } else if (!(await RsLoginHelper.isLoggedIn())) {
      await RsLoginHelper.login();
    }

    final mPath = '/rsMsgs/sendMail';
    final mParams = {
      'from': rsLocalState.authIdentityId,
      'to': to,
      'mailBody': jsonEncode(payload)
    };
    final response = await rsClient.apiCall(mPath, params: mParams);

    if (response['errorMsg'] is String && response['errorMsg'].length > 0) {
      throw Exception(response['errorMsg']);
    } else if (response['retval'] < to.length) {
      throw Exception('The message could not be delivered to all recipients');
    }

    if (!(response['trackingIds'] is List)) {
      throw Exception('The message could not be delivered');
    }

    var trackingIds = <String>[];
    for (var item in response['trackingIds'] ?? []) {
      if (item['mMailId'] is String) trackingIds.add(item['mMailId']);
    }

    return trackingIds;
  }

  /// Returns a list of {msgId, srcId, msgflags, msgtags}
  static Future<List<Map<String, dynamic>>> getMessageSummaries() async {
    final response = await rsClient.apiCall('/rsMsgs/getMessageSummaries');

    if (!(response is Map) || !(response['msgList'] is List)) {
      throw Exception('Could not retrieve the message summaries');
    }
    return response['msgList'].cast<Map<String, dynamic>>().toList() ?? [];
  }

  static Future<Map<String, dynamic>?> getMessage(String msgId) async {
    if (!(msgId is String) || msgId.isEmpty) {
      throw Exception('Invalid msgId');
    }

    final response =
        await rsClient.apiCall('/rsMsgs/getMessage', params: {'msgId': msgId});

    if (!(response is Map) || response['retval'] != true) return null;
    return response['msg'] ?? {};
  }

  static Future<bool> messageDelete(String msgId) async {
    if (!(msgId is String) || msgId.isEmpty) {
      throw Exception('Invalid msgId');
    }

    final response =
        await rsClient.apiCall('/rsMsgs/MessageDelete', params: {'msgId': msgId});

    if (!(response is Map)) throw Exception('Could not delete');
    return response['retval'] == true;
  }

  static Future<bool> createChatLobby(
      String lobbyName, String idToUse, String lobbyTopic,
      {List<Location> inviteList = const <Location>[],
      bool public = true,
      bool anonymous = true}) async {
    var req = ReqCreateChatLobby()
      ..lobbyName = lobbyName
      ..lobbyTopic = lobbyTopic
      ..lobbyIdentity = idToUse;
    if (inviteList.isNotEmpty) {
      req.invitedFriends =
          List.from(inviteList.map((location) => location.rsPeerId));
    }
    // Lobby flags
    // Public = 4
    // Public + signed = 20
    // Private = 0
    // Private + signed = 16
    var privacyType = 0;
    if (public && anonymous) {
      privacyType = 4;
    } else if (public && !anonymous) {
      privacyType = 20;
    } else if (!public && !anonymous) privacyType = 16;
    req.lobbyPrivacyType = privacyType;

    final response =
        await rsClient.apiCall('/rsMsgs/createChatLobby', params: req.toJson());
    if (response['retval']['xint64'] > 0) {
      setLobbyAutoSubscribe(response['retval']['xint64']);
      return true;
    }
    throw Exception('Failed to load response');
  }

  static Future<void> setLobbyAutoSubscribe(String lobbyId,
      [bool subs = true]) async {
    var chatLobbyId = ChatLobbyId();
    chatLobbyId.xstr64 = lobbyId;
    var params = {'lobby_id': chatLobbyId.toJson(), 'autoSubscribe': subs};

    final response =
        await rsClient.apiCall('/rsMsgs/setLobbyAutoSubscribe', params: params);
  }

  static Future<bool> getLobbyAutoSubscribe(String lobbyId,
      [bool subs = true]) async {
    var chatLobbyId = ChatLobbyId();
    chatLobbyId.xstr64 = lobbyId;
    var params = {'lobby_id': chatLobbyId.toJson()};

    final response =
        await rsClient.apiCall('/rsMsgs/setLobbyAutoSubscribe', params: params);
    return response['retval'];
  }

  static Future<void> unsubscribeChatLobby(String lobbyId) async {
    var chatLobbyId = ChatLobbyId();
    chatLobbyId.xstr64 = lobbyId;
    var params = {
      'lobby_id': chatLobbyId.toJson(),
    };

    final response =
        await rsClient.apiCall('/rsMsgs/unsubscribeChatLobby', params: params);
  }

  static Future<bool> sendMessage(String chatId, String msgTxt,
      [ChatIdType type = ChatIdType.number2_]) async {
    var id = ChatId();
    id.type = type;
    if (type == ChatIdType.number2_) {
      id.distantChatId = chatId;
    } else if (type == ChatIdType.number3_) {
      id.lobbyId = ChatLobbyId();
      id.lobbyId?.xstr64 = chatId;
    } else {
      throw ('Chat type not supported');
    }

    var params = {'id': id.toJson(), 'msg': msgTxt};
    final response = await rsClient.apiCall('/rsMsgs/sendChat', params: params);
    return response['retval'];
  }

  Future<bool> c(Chat chat) async {
    var params = {
      'to': chat.interlocutorId,
      'from': chat.ownIdToUse,
      'notify': true
    };
    final response =
        await rsClient.apiCall('/rsMsgs/initiateDistantChatConnexion', params: params);
    return response['retval'];
  }

  static Future<DistantChatPeerInfo> getDistantChatStatus(
      String pid, ChatMessage aaa) async {
    final response = await rsClient.apiCall('/rsMsgs/sendChat', params: {'pid': pid});
    if (response['retval'] != true) {
      throw ('Error on getDistantChatStatus()');
    }
    return DistantChatPeerInfo.fromJson(response['info']);
  }

  Future<List<VisibleChatLobbyRecord>> getUnsubscribedChatLobbies() async {
    var unsubscribedChatLobby = <VisibleChatLobbyRecord>[];
    var chatLobbies = await rsClient.apiCall('/rsMsgs/getListOfNearbyChatLobbies',
        params: {'pid': pid});
    for (VisibleChatLobbyRecord chat in chatLobbies['publicLobbies']) {
      var autosubs = await getLobbyAutoSubscribe(chat.lobbyId!.xstr64);
      if (!autosubs) {
        unsubscribedChatLobby.add(chat);
      }
    }
    return unsubscribedChatLobby;
  }
}

// ----------------------------------------------------------------------------
// Messages broadcasted to channels
// ----------------------------------------------------------------------------

class RsGxsChannel {
  static Future<void> subscribe(String channelId) async {
    final response = await rsClient.apiCall('/rsGxsChannels/subscribeToChannel',
        params: {'channelId': channelId, 'subscribe': true});

    if (!(response is Map) || response['retval'] != true) {
      throw Exception('Could not subscribe');
    }
  }

  static Future<void> unsubscribe(String channelId) async {
    final response = await rsClient.apiCall('/rsGxsChannels/subscribeToChannel',
        params: {'channelId': channelId, 'subscribe': false});

    if (!(response is Map) || response['retval'] != true) {
      throw Exception('Could not unsubscribe');
    }
  }

  /// Fetch the list of all contents, along with their ID, status and timestamp.
  /// Returns a map like { mMsgId: "", mmOrigMsgId: "", mMsgFlags: 0x1234, mMsgStatus: 0x1234 }
  static Future<List<Map<String, dynamic>>> getContentSummaries(
      String channelId) async {
    if (!(channelId is String) || channelId.isEmpty) {
      throw Exception('Invalid channel ID');
    }

    final response = await rsClient.apiCall('/rsGxsChannels/getContentSummaries',
        params: {'channelId': channelId});

    if (!(response is Map) || response['retval'] != true) {
      throw Exception('Could not subscribe');
    } else if (!(response['summaries'] is List)) {
      throw Exception('Invalid summaries');
    }

    return (response['summaries'] as List)
        .cast<Map<String, dynamic>>()
        .toList();
  }

  static Future<List<Map<String, dynamic>>> getChannelsSummaries() async {
    final response = await rsClient.apiCall('/rsGxsChannels/getChannelsSummaries');

    if (!(response is Map) || response['retval'] != true) {
      throw Exception('Could not retrieve the summary');
    } else if (!(response['channels'] is List)) {
      throw Exception('Invalid channels');
    }

    return (response['channels'] as List).cast<Map<String, dynamic>>().toList();
  }

  /// Fetches the given messages from the Channel
  static Future<List<Map<String, dynamic>>> getChannelItems(
      String channelId, List<String> msgIds) async {
    if (!(channelId is String) || channelId.isEmpty) {
      throw Exception('Invalid channel ID');
    } else if (!(msgIds is List))
      return getChannelContent(channelId); // all new

    final response = await rsClient.apiCall('/rsGxsChannels/getChannelContent',
        params: {'channelId': channelId, 'contentsIds': msgIds});

    if (!(response is Map) || response['retval'] != true) {
      throw Exception('Could not subscribe');
    } else if (!(response['posts'] is List)) throw Exception('Invalid posts');

    return (response['posts'] as List).cast<Map<String, dynamic>>().toList();
  }

  /// Fetches the relevant messages from the Channel
  static Future<List<Map<String, dynamic>>> getChannelContent(
      String channelId) async {
    final summaries = await getContentSummaries(channelId);
    final msgIds = summaries
        .map((item) => item['mMsgId'] ?? '')
        .toList()
        .cast<String>()
        .asMap();

    if (!(channelId is String) || channelId.isEmpty) {
      throw Exception('Invalid channel ID');
    }

    // TODO: filter by relevant stuff only

    final response = await rsClient.apiCall('/rsGxsChannels/getChannelContent',
        params: {'channelId': channelId, 'contentsIds': msgIds});

    if (!(response is Map) || response['retval'] != true) {
      throw Exception('Could not subscribe');
    } else if (!(response['posts'] is List)) throw Exception('Invalid posts');

    return (response['posts'] as List).cast<Map<String, dynamic>>().toList();
  }

  /// Sends a public message to all the channel, on the given thread ID.
  /// Returns the commentMessageId
  static Future<String> createComment(String channelId, String threadId,
      String identityId, Map<String, dynamic> payload) async {
    if (!(channelId is String) || channelId.isEmpty) {
      throw Exception('Invalid channel ID');
    } else if (!(threadId is String) || threadId.isEmpty) {
      throw Exception('Invalid thread ID');
    } else if (!(payload is Map)) throw Exception('Invalid comment payload');

    final response = await rsClient.apiCall('/rsGxsChannels/createCommentV2', params: {
      'authorId': identityId,
      'channelId': channelId,
      'threadId': threadId,
      'comment': payload,
      'parentId': threadId,
      // "origCommentId": "",
    });

    if (!(response is Map) || response['retval'] != true) {
      throw Exception('Could not create the comment');
    } else if (response['errorMessage'] is String &&
        response['errorMessage'].length > 0) {
      throw Exception(response['errorMessage']);
    } else if (!(response['commentMessageId'] is String)) {
      throw Exception('Invalid commentMessageId');
    }

    return response['commentMessageId'];
  }
}

// ----------------------------------------------------------------------------
// Forums
// ----------------------------------------------------------------------------

class RsGxsForum {
  static Future<String> createForumV2(String name,
      {String circleId = ''}) async {
    var circleType =
        circleId.isEmpty ? RsGxsCircleType.PUBLIC : RsGxsCircleType.EXTERNAL;

    final response = await rsClient.apiCall('/rsGxsForums/createForumV2', params: {
      'name': name,
      'circleType': circleType.index,
      'circleId': circleId
    });
    if (response['retval'] != true) {
      throw Exception('Forum could not be created.');
    }
    return response['forumId'];
  }

  static Future<String> createPost(
      String forumId, String title, String mBody, String authorId,
      [String parentId = '', String origPostId = '']) async {
    final response = await rsClient.apiCall('/rsGxsForums/createPost', params: {
      'forumId': forumId,
      'title': title,
      'mBody': mBody,
      'authorId': authorId,
      'parentId': parentId,
      'origPostId': origPostId
    });
    if (response['retval'] != true) {
      throw Exception('${response["errorMessage"]}');
    }
    return response['postMsgId'];
  }

  static Future<List<dynamic>> getForumsInfo(List<String> forumIds) async {
    final response = await rsClient.apiCall('/rsGxsForums/getForumsInfo',
        params: {'forumIds': forumIds});
    if (response['retval'] != true) {
      throw Exception('Could not retrieve forums info');
    }
    return response['forumsInfo'];
  }

  static Future<List<dynamic>> getForumsSummaries() async {
    final response = await rsClient.apiCall('/rsGxsForums/getForumsSummaries');
    if (response['retval'] != true) {
      throw Exception('Could not retrieve forum summaries');
    }
    return response['forums'];
  }

  static Future<List<RsMsgMetaData>> getForumMsgMetaData(String forumId) async {
    final response = await rsClient.apiCall('/rsGxsForums/getForumMsgMetaData',
        params: {'forumId': forumId});
    if (response['retval'] != true) {
      throw Exception('Could not retrieve messages metadata');
    }
    return [
      for (Map<String, dynamic> meta in response['msgMetas'])
        RsMsgMetaData.fromJson(meta)
    ];
  }

  static Future<List<RsGxsForumMsg>> getForumContent(
      String forumId, List<String> msgIds) async {
    final response = await rsClient.apiCall('/rsGxsForums/getForumContent',
        params: {'forumId': forumId, 'msgsIds': msgIds});
    if (response['retval'] != true) {
      throw Exception('Could not retrieve messages content');
    }
    return RsGxsForumMsg.listFromJson(response['msgs']);
  }

  static Future<bool> subscribeToForum(String forumId, [bool subscribe = true]) async {
    final response = await rsClient.apiCall('/rsGxsForums/subscribeToForum',
        params: {'forumId': forumId, 'subscribe': subscribe});
    if (response['retval'] != true) {
      throw Exception('Could not subscribe to forum: $response');
    }
    return response['retval'] == true;
  }

  static void requestSynchronization() async {
    try {
      rsClient.apiCall('/rsGxsForums/requestSynchronization');
    } catch (err) {
      print('/rsGxsForums/requestSynchronization not available $err');
    }
  }

  static Future<List<Map<String, dynamic>>> getChildPosts(
      String forumId, String parentId) async {
    final response = await rsClient.apiCall('/rsGxsForums/getChildPosts',
        params: {'forumId': forumId, 'parentId': parentId});
    if (response['retval']['errorNumber'] != 0) {
      throw Exception(
          'Could not retrieve child posts for $forumId/$parentId. Response: $response');
    }
    var childPosts = (response['childPosts'] as List).cast<Map<String, dynamic>>();
    return childPosts;
  }

  static Future<int> distantSearchRequest(String matchString) async {
    dbg('Starting distant search for: ' + matchString);

    final response = await rsClient.apiCall('/rsGxsForums/distantSearchRequest',
        params: {'matchString': matchString});
    if (response['retval']['errorNumber'] != 0) {
      throw Exception("Error: ${errToStr(response["retval"])}");
    }
    return response['searchId'];
  }

  static Future<List<dynamic>> localSearch(String matchString) async {
    dbg('Executing local search for: ' + matchString);

    final response = await rsClient.apiCall('/rsGxsForums/localSearch',
        params: {'matchString': matchString});
    if (response['retval']['errorNumber'] != 0) {
      throw Exception("Error: ${errToStr(response["retval"])}");
    }

    dbg('Results found:');
    dbg(response['searchResults'].toString());
    return response['searchResults'];
  }

  ///////////////////////////////////////////////////////////////
  // STORAGE PERIODS FOR POSTS
  ///////////////////////////////////////////////////////////////

  /// Get default maximum storage time for GXS messages
  ///
  /// [Storage] time in seconds
  static Future<int> getDefaultStoragePeriod() async {
    return (await rsClient.apiCall('/rsGxsForums/getDefaultStoragePeriod'))['retval'];
  }

  ///Get maximum storage time of GXS messages for the given group
  ///
  ///[groupId] Id of the group
  ///[storage] time in seconds
  static Future<int> getStoragePeriod(String groupId) async {
    return (await rsClient.apiCall(
        '/rsGxsForums/getStoragePeriod',
        params: {'groupId': groupId,})
    )['retval'];
  }

  /// Set GXS messages maximum storage time for the given group
  ///
  /// [groupId] Id of the group
  /// [storageSecs] storage time in seconds
  static Future<void> setStoragePeriod(String groupId, int storageSecs) async {
    await rsClient.apiCall(
        '/rsGxsForums/setStoragePeriod',
        params: {'groupId': groupId, 'storageSecs': storageSecs}
    );
  }

  /// Get default maximum syncronization age for GXS messages
  ///
  /// age in seconds
  static Future<int> getDefaultSyncPeriod() async {
    return (await rsClient.apiCall('/rsGxsForums/getDefaultSyncPeriod'))['retval'];
  }

  /// Get maximum syncronization age of GXS messages for the given group
  ///
  /// [groupId] Id of the group
  /// return age in seconds
  static  Future<int> getSyncPeriod(String groupId) async {
    return (await rsClient.apiCall(
        '/rsGxsForums/getSyncPeriod',
        params: {'groupId': groupId,})
    )['retval'];
  }


  /// Set GXS messages maximum syncronization age for the given group
  ///
  /// [groupId] Id of the group
  /// [syncAge] age in seconds
  static  Future<void> setSyncPeriod(String groupId, int syncAge) async {
    await rsClient.apiCall(
        '/rsGxsForums/setSyncPeriod',
        params: {'groupId': groupId, 'storageSecs': syncAge}
    );
  }

}


// ----------------------------------------------------------------------------
// Files
// ----------------------------------------------------------------------------

class RsFiles {
// period defaults to 10 years
  static Future<bool> extraFileHash(String localPath,
      [int period = 31536000 * 10, int flags = 0x40]) async {
    final response = await rsClient.apiCall('/rsFiles/ExtraFileHash', params: {
      'localpath': localPath,
      'period': {'xstr64': period.toString()},
      'flags': flags
    });
    if (response['retval'] != true) {
      throw Exception('File hash process failed.');
    }
    return response['retval'] == true;
  }

  static Future<Map> extraFileStatus(String localPath) async {
    final response = await rsClient.apiCall('/rsFiles/ExtraFileStatus',
        params: {'localpath': localPath});
    if (response['retval'] != true) {
      print('Could not retrieve file status for $localPath');
    }
    return response['info'];
  }

  static Future<String> exportFileLink(
      String fileHash, int fileSize, String fileName) async {
    final params = {
      'fileHash': fileHash,
      'fileSize': fileSize,
      'fileName': fileName
    };

    final response = await rsClient.apiCall('/rsFiles/exportFileLink', params: params);
    if (response['retval']['errorNumber'] != 0) {
      throw Exception('Could not export file link. $params');
    }

    return response['link'];
  }

  /// Parse RetroShare files link
  ///
  /// Support also old RetroShare-gui file and collections links format.
  /// [link] files link either in base64 or URL format
  /// Return collection storage for parsed files link
  /// error information if some error occurred, 0/SUCCESS otherwise
  static Future<Map> parseFilesLink(String link) async {
    final response =
        await rsClient.apiCall('/rsFiles/parseFilesLink', params: {'link': link});
    if (response['retval']['errorNumber'] != 0) {
      throw Exception('Could not parse file link: $link');
    }

    return response['collection'];
  }

  /// Initiate download of a files collection
  ///
  /// An usually useful companion method of this is @see parseFilesLink()
  /// collection collection of files to download
  /// destPath optional base path on which to download the 	collection, if left
  /// empty the default download directory will be used
  /// NOT SUPPORTED srcIds optional peers id known as direct source of the
  ///	collection
  /// NOT SUPPORTED flags optional flags to fine tune search and download
  ///	algorithm
  /// NOT SUPPORTED return success or error details.
  static Future<bool> requestFiles(Map collection) async {
    print('Requesting $collection');
    final response = await rsClient.apiCall('/rsFiles/requestFiles',
        params: {'collection': collection});
    if (response['retval']['errorNumber'] != 0) {
      throw Exception('Files request failed.');
    }
    return response['retval']['errorNumber'] == 0;
  }

  static Future<void> setDownloadDirectory(String path) async {
    final response = await rsClient.apiCall('/rsFiles/setDownloadDirectory',
        params: {'path': path});
    if (response['retval'] != true) {
      throw Exception('Error setting download directory. Response: $response');
    }
  }

  static Future<String> getDownloadDirectory() async {
    var response = await rsClient.apiCall('/rsFiles/getDownloadDirectory');
    print('Download dir: $response');
    if (!(response is Map)) throw FormatException();
    return response['retval'];
  }

  static Future<String> getPartialsDirectory() async {
    var response = await rsClient.apiCall('/rsFiles/getPartialsDirectory');
    print('Partials dir: $response');
    if (!(response is Map)) throw FormatException();
    return response['retval'];
  }

  static Future<List> getSharedDirectories() async {
    var response = await rsClient.apiCall('/rsFiles/getSharedDirectories');
    print('Shared dirs: $response');
    if (response['retval'] != true) {
      throw Exception('Error getting shared directories. Response: $response');
    }
    return response['dirs'];
  }

  /// Remove directory from shared list
  ///
  /// [dir] Path of the directory to remove from shared list
  /// return false if something failed, true otherwhise
  static Future<bool> removeSharedDirectory(String dir) async =>
    (await rsClient.apiCall('/rsFiles/removeSharedDirectory', params: {'dir': dir}))['retval'];


  static Future<void> setPartialsDirectory(String path) async {
    final response = await rsClient.apiCall('/rsFiles/setPartialsDirectory',
        params: {'path': path});
    if (response['retval'] != true) {
      throw Exception(
          'Error setting partial download directory. Response: $response');
    }
  }

  /// Add shared directory
  ///
  /// [filename] is the directory or file you want to share. [virtualname] is
  /// the name will be shown on RS, if null will be `basename(filename)`. For
  /// [shareflags] read RS documentation.
  ///
  /// return false if something failed or already shared, true if shared
  static Future<bool> addSharedDirectory(String filename,
      {String? virtualname, int shareflags = 0x80}) async {
    print('Adding shared directory: ');

    virtualname ??= basename(filename);

    final response = await rsClient.apiCall('/rsFiles/addSharedDirectory', params: {
      'dir': {
        'filename': filename,
        'virtualname': virtualname,
        'shareflags': shareflags
      }
    });

    if (response['retval'] != true) {
      throw Exception(
          'Error adding shared directory $filename . $response\nMay be already added');
    }
    return response['retval'];
  }

  /// Clear completed downloaded files list
  ///
  /// false on error, true otherwise
  static Future<bool> fileClearCompleted() async =>
      (await rsClient.apiCall('/rsFiles/fileClearCompleted'))['retval'];
  

  /// Get incoming files list
  ///
  /// @param[out] hashs storage for files identifiers list
  static Future<List> fileDownloads() async {
    final response = await rsClient.apiCall('/rsFiles/FileDownloads');
    return response['hashs'];
  }

  /// Request directory details, subsequent multiple call may be used to
  /// explore a whole directory tree.
  ///
  /// @param[out] details Storage for directory details
  /// @param[in] handle element handle 0 for root, pass the content of
  ///	DirDetails::child[x].ref after first call to explore deeper, be aware
  ///	that is not a real pointer but an index used internally by RetroShare.
  /// @param[in] NOTSUPORTED flags file search flags RS_FILE_HINTS_*
  /// @return false if error occurred, true otherwise
  static Future<DirDetails> requestDirDetails([String? handle]) async {
    Map response;
    if (handle == null) {
      response = await rsClient.apiCall('/rsFiles/requestDirDetails');
    } else {
      response = await rsClient.apiCall('/rsFiles/requestDirDetails', params: {
        // 'handle': handle
       "handle": {"xstr64": handle}
      });
    }
    if (response['retval'] != true) {
      throw Exception('Error requesting Dir Details.');
    }
    return DirDetails.fromJson(response['details']);
  }

  /// Controls file transfer
  ///
  /// [hash] file identifier
  /// [flags] action to perform. Pict into
  /// { RS_FILE_CTRL_PAUSE, RS_FILE_CTRL_START, RS_FILE_CTRL_FORCE_CHECK } }
  ///
  /// Check [RS_FILE_CTRL] for flags.
  ///
  /// false if error occurred such as unknown hash.
  static Future<bool> FileControl(String hash, int flags) async =>
      (await rsClient.apiCall('/rsFiles/FileControl',
          params: {'hash': hash, 'flags': flags} ))['retval'];

  /// Cancel file downloading
  ///
  /// [hash] of de file to cancel
  /// false if the file is not in the download queue, true otherwhise
  static Future<bool> FileCancel(String hash) async =>
      (await rsClient.apiCall('/rsFiles/FileCancel',
          params: {'hash': hash} ))['retval'];

  /// Get file details
  ///
  /// [hash] file identifier
  /// [hintflags] filtering hint ( RS_FILE_HINTS_UPLOAD|...| RS_FILE_HINTS_EXTRA
  /// |RS_FILE_HINTS_LOCAL )
  /// Out info storage for file information
  /// return true if file found, false otherwise
  ///
  /// WARNING: for a downloaded file could return false
  static Future<Map> fileDetails(String hash, [int hintflags = 0x10]) async {
    final response = await rsClient.apiCall('/rsFiles/FileDetails',
        params: {'hash': hash, 'hintflags': hintflags});
    if (response['retval'] != true) {
      print('File with hash $hash not found. Retval: ${response["retval"]}');
    }
    return response['info'];
  }

  /// Check if we already have a file
  ///
  /// [hash] SHA1 file identifier
  /// Return info storage for the possibly found file information
  static Future<Map?> alreadyHaveFile(String hash) async {
    final response =
        await rsClient.apiCall('/rsFiles/alreadyHaveFile', params: {'hash': hash});
    if (response['retval'] != true) {
      return null;
    }
    return response['info'];
  }

  /// Remove file from extra file shared list
  ///
  /// [hash] hash of the file to remove
  /// return false on error, true otherwise
  static Future<bool> extraFileRemove(String hash) async =>
      (await rsClient.apiCall('/rsFiles/extraFileRemove', params: {'hash': hash}))['retval'];

  /// Search the whole reachable network for similar file
  ///
  /// An [RsPerceptualSearchResultEvent] is emitted when matching results
  ///	arrives from the network
  /// [localFilePath] path of local file to search for similarity on
  ///	the network
  /// [distance] maximum hamming distance tolerated to consider a file
  ///	similar
  /// Returns [searchId] storage for search id, useful to track search events
  ///	and retrieve search results
  static Future<int> perceptualSearchRequest(
      String localFilePath, int distance) async {
    print('Loading file for perceptual search: $localFilePath');
    var response = await rsClient.apiCall('/rsFiles/perceptualSearchRequest',
        params: {'localFilePath': localFilePath, 'distance': distance});
    print('perceptualSearchRequest');
    print(response);
    if (!(response is Map)) {
      throw FormatException();
    } else if (response['retval']['errorNumber'] != 0) {
      throw Exception(response['retval']);
    }
    return response['searchId'];
  }

  /// Force shared directories check
  /// [add_safe_delay] Schedule the check 20 seconds from now, to ensure to capture files written just now.
  static Future<void> forceDirectoryCheck(
          {bool add_safe_delay = false}) async =>
      await rsClient.apiCall('/rsFiles/forceDirectoryCheck',
          params: {'add_safe_delay': add_safe_delay});
}

class RsJsonApi {
  static Future<bool> authorizeUser(String user, String password) async {
    final response = await rsClient.apiCall('/rsJsonApi/authorizeUser',
        params: {'user': user, 'password': password});

    if (!(response is Map)) throw Exception('Error on the response');

    return response['retval']['errorNumber'] == 0
        ? true
        : throw Exception('Error authorizing token');
  }

  static Future<Map> version() async {
    return await rsClient.apiCall('/rsJsonApi/version');
  }
}

// ----------------------------------------------------------------------------
// Config
// ----------------------------------------------------------------------------

class RsConfig {
  static Future<Map> getMaxDataRates() async {
    var response = await rsClient.apiCall('/rsConfig/GetMaxDataRates');
    if (response['retval'] != 1) print('Could not get data rates');
    final rates = {'inKb': response['inKb'], 'outKb': response['outKb']};
    return rates;
  }

  static Future<Map> setMaxDataRates(int downKb, int upKb) async {
    var response = await rsClient.apiCall('/rsConfig/SetMaxDataRates',
        params: {'downKb': downKb, 'upKb': upKb});
    if (response['retval'] != 1) print('Could not set data rates $response');
    return response;
  }
}

// ----------------------------------------------------------------------------
// Circles
// ----------------------------------------------------------------------------

class RsGxsCircles {

  /// Create new circle
  ///
  /// [circleName] String containing cirlce name
  /// [circleType] Circle type
  /// [restrictedId] Optional id of a pre-existent circle that see the
  ///	created circle. Meaningful only if circleType == EXTERNAL, must be null
  ///	in all other cases.
  /// [authorId] Optional author of the circle.
  /// [gxsIdMembers] GXS ids of the members of the circle.
  /// [localMembers] PGP ids of the members if the circle.
  /// Returns circleId Optional storage to output created circle id
  static Future<String> createCircle(
    String circleName,
    RsGxsCircleType circleType, {
    String restrictedId = '',
    List<String> gxsIdMembers = const [],
    List<String> localMembers = const [],
  }) async {
    var response = await rsClient.apiCall('/rsGxsCircles/createCircle', params: {
      'circleName': circleName,
      'circleType': circleType.index,
      'restrictedId': restrictedId,
      'gxsIdMembers': gxsIdMembers,
      'localMembers': localMembers,
    });

    if (!(response is Map)) {
      throw Exception('Could not retrieve the details');
    } else if (response['retval'] != true) {
      throw Exception('Error creating circle $circleName . $response');
    }

    return response['circleId'];
  }

  /// Request circle membership, or accept circle invitation
  ///
  /// [ownGxsId] Id of own identity to introduce to the circle.
  /// Default value actual [authIdentityId]
  /// [circleId] Id of the circle to which ask for inclusion
  /// return false if something failed, true otherwhise
  static Future<bool> requestCircleMembership(String circleId,
      [String? ownGxsId]) async {
    if (!(circleId is String) || circleId.isEmpty) {
      throw Exception('Invalid circle ID');
    }

    final response = await rsClient.apiCall('/rsGxsCircles/requestCircleMembership',
        params: {'ownGxsId': ownGxsId ?? rsLocalState.authIdentityId, 'circleId': circleId});

    if (!(response is Map)) throw Exception('Could not subscribe');
    return response['retval'];
  }

  /// Invite identities to circle (admin key is required)
  ///
  /// [identities] ids of the identities to invite
  /// [circleId] Id of the circle you own and want to invite ids in
  /// return false if something failed, true otherwhise
  static Future<bool> inviteIdsToCircle(
      List<String> identities, String circleId) async {
    print('Inviting ids ' + identities.toString() + ' to circleId');
    final response = await rsClient.apiCall('/rsGxsCircles/inviteIdsToCircle',
        params: {'identities': identities, 'circleId': circleId});

    if (!(response is Map)) {
      throw Exception('Could not invite Ids $identities to circle $circleId');
    }
    return response['retval'];
  }

  /// Get circle details. Memory cached
  ///
  /// [id] Id of the circle
  /// return details Storage for the circle details
  static Future<Map<String, dynamic>> getCircleDetails(String id) async {
    if (id.isEmpty) throw Exception('Invalid circle ID');

    final response =
        await rsClient.apiCall('/rsGxsCircles/getCircleDetails', params: {'id': id});

    if (!(response is Map)) {
      throw Exception('Could not retrieve the details');
    } else if (response['retval'] != true) {
      throw Exception("Can't retrieve details for $id");
    }
    return response['details'];
  }

  /// Get circles summaries list.
  ///
  /// return circles list where to store the circles summaries
  static Future<List<Map<String, dynamic>>> getCirclesSummaries() async {
    print('Starting getCirclesSummaries');
    final response = await rsClient.apiCall('/rsGxsCircles/getCirclesSummaries');
    if (response['retval'] != true) {
      throw Exception('Could not retrieve circle summaries');
    }
    return response['circles'].cast<Map<String, dynamic>>().toList() ?? [];
  }

  /// Get circles information
  ///
  /// [circlesIds] ids of the circles of which to get the informations
  /// return [circlesInfo] storage for the circles informations
  static Future<List<dynamic>> getCirclesInfo(List<String> circlesIds) async {
    final response = await rsClient.apiCall('/rsGxsCircles/getCirclesInfo',
        params: {'circlesIds': circlesIds});
    if (!(response is Map)) {
      throw Exception('Error calling getCirclesInfo');
    } else if (!response['retval']) {
      throw Exception("Can't getCirclesInfo for " + circlesIds.toString());
    }
    return response['circlesInfo'];
  }

  /// Remove identities from circle (admin key is required)
  ///
  /// [identities] ids of the identities to remove from the invite list
  /// [circleId] Id of the circle you own and want to revoke identities
  /// return false if something failed, true otherwhise
  static Future<bool> revokeIdsFromCircle(
      List<String> identities, String circleId) async {
    final response = await rsClient.apiCall('/rsGxsCircles/revokeIdsFromCircle',
        params: {'identities': identities, 'circleId': circleId});
    if (!(response is Map)) {
      throw Exception('Error revoking $identities from $circleId');
    }
    return response['retval'];
  }

  /// Leave given circle
  ///
  /// [ownGxsId] Own id to remove from the circle. Default value actual [authIdentityId]
  /// [circleId] Id of the circle to leave
  /// return false if something failed, true otherwhise
  static Future<bool> cancelCircleMembership(String circleId,
      [String? ownGxsId]) async {
    final response = await rsClient.apiCall('/rsGxsCircles/cancelCircleMembership',
        params: {'ownGxsId': ownGxsId ?? rsLocalState.authIdentityId, 'circleId': circleId});
    if (!(response is Map)) {
      throw Exception('Error cancelCircleMembership $ownGxsId from $circleId');
    }
    return response['retval'];
  }

  /// Edit own existing circle
  ///
  /// Parameter inout [circleInfo] is the same object that the recieved with
  /// getCirclesInfo. Circle data with modifications, storage for data
  ///	updatedad during the operation.
  /// Return a circle info.
  static Future<Map<String, dynamic>> editCircle(
      Map<String, dynamic> circleInfo) async {
    final response = await rsClient.apiCall('/rsGxsCircles/editCircle',
        params: {'cData': circleInfo});
    if (!(response is Map)) {
      throw Exception('Error editCircle $circleInfo');
    } else if (!response['retval']) {
      throw Exception('Could not edit editCircle $circleInfo');
    }
    return response['cData'];
  }
}

