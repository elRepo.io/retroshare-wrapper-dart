/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2022  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2022 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'dart:convert';

import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:retroshare_dart_wrapper/retroshare.dart';
import 'package:retroshare_dart_wrapper/src/exceptions.dart';
import 'package:test/test.dart';
import 'package:http/http.dart';


@GenerateMocks([Client])
import 'api_client_test.mocks.dart';

@GenerateMocks([RsApiClient])
import 'api_client_test.mocks.dart';

void main() {

  group('RsApiClient with mocked http client', () {
    late MockClient mockClient;

    setUp(() async {
      mockClient = MockClient();
      rsClient.client = mockClient;
    });

    test('Api call with 200 status code', () async {
      when(
          mockClient.post(any, headers: anyNamed('headers'), body: anyNamed('body'))
      ).thenAnswer(
              (realInvocation) async => Response('{"retval": true}', 200)
      );

      // Random api call to test mocked value is correct
      expect(await RsFiles.FileCancel('test'), true);

    });

    test('Api call with 401 status code', () async {
      when(
          mockClient.post(any, headers: anyNamed('headers'), body: anyNamed('body'))
      ).thenAnswer(
              (realInvocation) async => Response('{"retval": false}', 401)
      );
      expect(RsFiles.FileCancel('test'), throwsA( TypeMatcher<AuthFailedException>() ));
    });

    test('test specific endpoint return the correct value', () async {
      var hash = 'test';
      when(
          mockClient.post(
            Uri.parse(rsClient.getCompletePath('/rsFiles/FileCancel')),
            headers: anyNamed('headers'),
            body: argThat(equals(jsonEncode({'hash': hash})), named: 'body'),
          )
      ).thenAnswer(
              (realInvocation) async => Response('{"retval": true}', 200)
      );
      expect(await RsFiles.FileCancel(hash), true);
    });

  });

  group('RsApiClient with mocked rsApiClient', () {
    late MockRsApiClient mockRsApiClient;

    setUp(() async {
      mockRsApiClient = MockRsApiClient();
      rsClient = mockRsApiClient;
    });

    test('test specific endpoint return the correct value', () async {
      var hash = 'test';
      when(
          mockRsApiClient.apiCall(
            '/rsFiles/FileCancel',
            params: argThat(equals({'hash': hash}), named: 'params'),
          )
      ).thenAnswer(
              (realInvocation) async => {'retval': true}
      );

      expect(await RsFiles.FileCancel(hash), true);
    });


  });

  // todo: unimplemented
  group('EventsHandler', () {
  });

}