/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'dart:io';

import 'package:retroshare_dart_wrapper/rs_models.dart';
import 'package:test/test.dart';

import 'elrepo_lib_test.dart';
import 'package:retroshare_dart_wrapper/retroshare.dart' as rs;
import 'package:path/path.dart';

/// How to run tests:
/// 1. adb forward tcp:9091 tcp:9092
/// 2. Modify [fileToShare] and [fileHash] variables
///
/// - Run [shareFile] to share a file on RS:
/// 
/// ```
/// pub run test test/sharing_files_test.dart -N shareFile
/// ```
///
/// - Run [fileExistByHash] to check if a hash exists on `alreadyHaveFile` and
/// on the RS file tree.
///
/// ```
/// pub run test test/sharing_files_test.dart -N fileExistByHash
/// ```


// File hash of a file to check if exist
var fileHash = '85d5043f00a1938bac45cda53ec1152428d00a3a';
var fileToShare =
    '/storage/emulated/0/DCIM/Screenshots/Screenshot_2021-08-19-16-39-51-251_com.example.pareu_de_pararme_map.jpg';

var fileLink =
    "retroshare:///files?filesCount=1&filesData=ARdJTUdfMjAyMTA1MjZfMDczOTA0LmpwZ7a13QKF1QQ_AKGTi6xFzaU-wRUkKNAKOgEBLwABAAG2td0C&filesName=IMG_20210526_073904.jpg&filesSize=5724854";
// Set specific maximum test duration for downloading a file
var _downloadTimeout = Duration(minutes: 10);

/// Search recursivelly on the RS directory tree until find a specified path or
/// file returning it information
Future<DirDetails> _printDiretoryTree([int handle = 0]) async {
  var directory = await rs.RsFiles.requestDirDetails(handle);
  if (directory.children.isNotEmpty) {
    for (var child in directory.children) {
      print(child);
      var childDetails =
      await _printDiretoryTree(child['handle']);
      if (childDetails != null) return childDetails;
    }
  }
  return null;
}

void main() {
  group('Test file share bugs', () {
    // Test share a file event is not fired
    test('shareFile', () async {
      AUTH.initRemote();
      var fileHash;

      // Check if already shared
      if (await rs.isDirectoryAlreadyShared(fileToShare)) {
        print('- The file was already shared');
        // If dirInfo here is null could mean that the file is not already hashed
        var dirInfo = await rs.findAFileOnDiretoryTree(fileToShare);
        if (dirInfo == null) throw ('Probably the file is not hashed already');
        fileHash = dirInfo.hash;
      }
      // If is not shared we are going to wait until file hash event is fired
      else {
        print('- Hashing new file');
        final hashingFile =
          await rs.RsFiles.addSharedDirectory(fileToShare, shareflags: 2176);
        if (hashingFile) fileHash = await rs.waitForFileHash(fileToShare);
      }
      assert(fileHash != null);
      print("RESULT: $fileHash $fileToShare");
    });

    test('fileExistByHash', () async {
      AUTH.initRemote();

      print('Printing directory tree for debugging');
      print('-------------------------------------');
      await _printDiretoryTree();
      print('-------------------------------------');
      print('Running tests: ');

      // Test already have a file
      var res = await rs.RsFiles.alreadyHaveFile(fileHash);
      print('- alreadyHaveFile response: ${res!=null}');
      assert(res != null);


      // Search it on file tree
      var dirInfo = await rs.findAFileOnDiretoryTree(res['fname']);
      print("- Found file on directory tree: ${dirInfo!=null}");
      assert(dirInfo != null);

      // Try to get file info via file details will fail if already have a file
      // is true
      // res = await rs.RsFiles.fileDetails(fileHash);
      // print('Already fileDetails: $res');
      // assert(res['hash'] == fileHash);
    });

    test('fileDownloadWorkflow', () async {
      AUTH.initRemote();

      var parsedLink = await rs.RsFiles.parseFilesLink(fileLink);
      var hashFromLink = parsedLink['mFiles'][0]['hash'];

      // Remove file if you already have it
      var fileInfo = await rs.RsFiles.alreadyHaveFile(hashFromLink);
      if(fileInfo != null)  {
        // todo(kon) manually do this using adb
        throw('File ${fileInfo["path"]} already shared, remove it or restart RS node\n'
            'adb shell rm ${fileInfo["path"]}\n'
            'adb shell am force-stop net.altermundi.elrepoio'); // Do it using Intents to stop the foreground service properly
        File(fileInfo['path']).delete();
        // assert(await rs.RsFiles.removeSharedDirectory(fileInfo['path']));
        fileInfo = null;
      }

      // Download the file from RS network again
      print("Requesting: $parsedLink");
      assert(await rs.RsFiles.requestFiles(parsedLink));

      // Await download
      var _progress = 0.0;
      do {
        var downloadDetails = await rs.RsFiles.fileDetails(hashFromLink);
        _progress =
            downloadDetails['avail']['xint64'] / downloadDetails['size']['xint64'];
        print('progress for $hashFromLink: $_progress');
        if (_progress >= 1.0) {
          print('download finished.');
        }
        await Future.delayed(Duration(seconds: 1));
      } while (_progress <1.0);

      // Await RS do some stuff after download
      print("After download...");
      String filePath;
      Map<dynamic, dynamic> fileDetails, alreadyHaveFile;
      do {
        alreadyHaveFile = await rs.RsFiles.alreadyHaveFile(hashFromLink);
        filePath = alreadyHaveFile is Map ? alreadyHaveFile['path'] : null;
        fileDetails = await rs.RsFiles.fileDetails(hashFromLink); // Used to get the download status
        print('Already have a file $filePath '
            '\nDownload status: ${fileDetails["downloadStatus"]}'
            '\nPath from fileDetails: ${fileDetails['path']}');
        await Future.delayed(Duration(seconds: 1));
      } while (filePath == null);

      assert(fileDetails['downloadStatus'] == 4);
      assert(alreadyHaveFile['hash'] == hashFromLink);

      print('- Running fileClearCompleted');
      assert(await rs.RsFiles.fileClearCompleted());
      var hashes = await rs.RsFiles.fileDownloads();
      await Future.delayed(Duration(seconds: 2));

      alreadyHaveFile = null;
      alreadyHaveFile = await rs.RsFiles.alreadyHaveFile(hashFromLink);
      fileDetails = await rs.RsFiles.fileDetails(hashFromLink);

      print('Already have a file ${alreadyHaveFile} '
          '\nFileDetails: ${fileDetails}');

      assert(alreadyHaveFile['hash'] == hashFromLink);
      assert(fileDetails['downloadStatus'] == 4);

    }, timeout: Timeout(_downloadTimeout));
  });

}