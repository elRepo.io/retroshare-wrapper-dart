/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'dart:io';

import 'package:retroshare_dart_wrapper/retroshare.dart' as rs;

class AUTH {
  static const String identityId = 'bdbd397cf7800c5e085968e185633b50';
  static const String locationId = '814228577bc0c5da968c79272adcbfce';
  static const String passphrase = 'test';
  static const String apiUser = 'test';
}

Future<void> main() async {

  // To use this flow make sure you have an account created and a valid elrepo.io
  // token to use.
  // We tested it on a retroshare-service running on the host

  // Login flow
  final location = await rs.RsLoginHelper.getDefaultLocation();
  final isLoggedIn = await rs.RsLoginHelper.isLoggedIn();
  print('Default location: $location');
  print('Enter password: ');
  final password = stdin.readLineSync();
  if (isLoggedIn) {
    print('Rs is logged in');
  } else {
    print('Logging in... await');
    if ((await rs.RsLoginHelper.login(location, password)) != 0) {
      print('Bad login attempt.');
      return;
    }
    print('Logged in');
  }

  var res = await rs.RsGxsForum.getForumsSummaries();
  print('Forum Summaries: $res');
}
